# Docker images

[[_TOC_]]

## Alpine

```
registry.gitlab.com/hranicka/docker/alpine
```

This image provides minimal Docker image suited mostly for application deployment.

Based on Alpine linux, includes SSH client and rsync.

## NGINX

```
registry.gitlab.com/hranicka/docker/nginx
registry.gitlab.com/hranicka/docker/nginx:alpine
```

This image provides preconfigured nginx for PHP-FPM application.

Requires the PHP application running on `app` host.

#### Example

```yaml
# docker-compose.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-fpm-alpine
    volumes:
      - ./:/var/www/html
      - composer-data:/var/www/.composer

  web:
    image: registry.gitlab.com/hranicka/docker/nginx:alpine
    volumes:
      - ./public:/var/www/html/public

volumes:
  composer-data:
```

```yaml
# docker-compose.override.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-fpm-alpine-dev
    environment:
      XDEBUG_MODE: "debug"
      XDEBUG_CONFIG: "discover_client_host=true idekey=PHPSTORM log_level=0"
      PHP_IDE_CONFIG: "serverName=localhost"

  web:
    ports:
      - "8080:80"
```

## Apache (httpd)

```
registry.gitlab.com/hranicka/docker/httpd:2.4
```

This image provides preconfigured httpd/apache for PHP application.

Requires the PHP application running on `app` host.

#### Example

```yaml
# docker-compose.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-fpm-alpine
    volumes:
      - ./:/var/www/html
      - composer-data:/var/www/.composer

  web:
    image: registry.gitlab.com/hranicka/docker/httpd:2.4-alpine
    volumes:
      - ./public:/var/www/html/public

volumes:
  composer-data:
```

```yaml
# docker-compose.override.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-fpm-alpine-dev
    environment:
      XDEBUG_MODE: "debug"
      XDEBUG_CONFIG: "discover_client_host=true idekey=PHPSTORM log_level=0"
      PHP_IDE_CONFIG: "serverName=localhost"

  web:
    ports:
      - "8080:80"
```

## PHP

### PHP-CLI

```
registry.gitlab.com/hranicka/docker/php:8.4-cli
registry.gitlab.com/hranicka/docker/php:8.4-cli-dev
registry.gitlab.com/hranicka/docker/php:8.4-cli-alpine
registry.gitlab.com/hranicka/docker/php:8.4-cli-alpine-dev
```

Images suitable for CLI applications.

### PHP-FPM

```
registry.gitlab.com/hranicka/docker/php:8.4-fpm
registry.gitlab.com/hranicka/docker/php:8.4-fpm-dev
registry.gitlab.com/hranicka/docker/php:8.4-fpm-alpine
registry.gitlab.com/hranicka/docker/php:8.4-fpm-alpine-dev
```

Images suitable for PHP-Nginx stack.

#### Example

```yaml
# docker-compose.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-fpm-alpine
    volumes:
      - ./:/var/www/html
      - composer-data:/var/www/.composer

  web:
    image: registry.gitlab.com/hranicka/docker/nginx:alpine
    volumes:
      - ./public:/var/www/html/public

volumes:
  composer-data:
```

```yaml
# docker-compose.override.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-fpm-alpine-dev
    environment:
      XDEBUG_MODE: "debug"
      XDEBUG_CONFIG: "discover_client_host=true idekey=PHPSTORM log_level=0"
      PHP_IDE_CONFIG: "serverName=localhost"

  web:
    ports:
      - "8080:80"
```

### PHP-RoadRunner

```
registry.gitlab.com/hranicka/docker/php:8.4-rr
registry.gitlab.com/hranicka/docker/php:8.4-rr-dev
```

Images for maximal performance with [RoadRunner](https://roadrunner.dev/) instead of Nginx.

This requires to install [RoadRunner Symfony Bundle](https://github.com/baldinof/roadrunner-bundle).
See the bundle readme since some modifications to the application are necessary!

#### Example

```yaml
# docker-compose.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-rr
    volumes:
      - ./:/var/www/html
      - composer-data:/var/www/.composer
      #- certs:/etc/certs

volumes:
  composer-data:
  #certs:
```

```yaml
# docker-compose.override.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/php:${PHP_VERSION-8.4}-rr
    environment:
      XDEBUG_MODE: "debug"
      XDEBUG_CONFIG: "discover_client_host=true idekey=PHPSTORM log_level=0"
      PHP_IDE_CONFIG: "serverName=localhost"
    ports:
      - "8080:8080"
```

### Go

```
registry.gitlab.com/hranicka/docker/go:1.24
registry.gitlab.com/hranicka/docker/go:1.24-alpine
```

Images intended to be used for a Go application.

#### Example

```yaml
# docker-compose.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/go:${GO_VERSION-1.24}-alpine
    volumes:
      - ./:/usr/src/app
```

```yaml
# docker-compose.override.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/go:${GO_VERSION-1.24}-alpine
    ports:
      - "8080:8080" # regular go server
```

### Node.js

```
registry.gitlab.com/hranicka/docker/node:22
registry.gitlab.com/hranicka/docker/node:22-alpine
```

Images intended to be used for a Node.js application.

#### Example

```yaml
# docker-compose.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/node:${NODE_VERSION-22}-alpine
    volumes:
      - ./:/usr/src/app
```

```yaml
# docker-compose.override.yml
version: '3'

services:
  app:
    image: registry.gitlab.com/hranicka/docker/node:${NODE_VERSION-22}-alpine
    ports:
      - "5173:5173" # vite server
      - "8080:8080" # regular node.js application
```
