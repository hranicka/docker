server {
    listen 80;

    root /var/www/html/public;

    # serve static files without any redirection
    location ~ ^/(build|assets) {
        try_files $uri @notFound;
        add_header "Cache-Control" "max-age=604800, public" always;
    }
    location @notFound {
        internal;
        add_header "Cache-Control" "no-cache" always;
        return 404;
    }

    # try to serve file directly, fallback to index.php
    location / {
        try_files $uri /index.php$is_args$args;
    }
    location ~ ^/index\.php(/|$) {
        # config for a Docker Compose/Swarm stack
        fastcgi_pass app:9000;

        # config for Kubernetes - as a service
        # fastcgi_pass app.default.svc.cluster.local:9000;
        # config for Kubernetes - container within a Pod
        # fastcgi_pass localhost:9000;

        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        # optionally set the value of the environment variables used in the application
        # fastcgi_param APP_ENV prod;
        # fastcgi_param APP_DEBUG false;
        # fastcgi_param APP_SECRET <app-secret-id>;
        # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }
}
