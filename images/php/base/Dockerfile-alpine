ARG BASE_IMAGE
FROM $BASE_IMAGE

# copy binaries from other images
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/

# move www-data user&group to uid/gid 1000 to easy share permissions with host system
RUN apk add --no-cache shadow && \
    usermod -u 1000 www-data && \
    groupmod -g 1000 www-data

# fix permissions from root
RUN mkdir -p /var/www/.composer && chown -R www-data:www-data /var/www/.composer
RUN mkdir -p /var/www/html && chown -R www-data:www-data /var/www/html

# install dependencies
RUN apk add --no-cache tzdata git unzip

# set correct timezone
ENV TZ="Europe/Prague"

RUN install-php-extensions \
    bcmath \
    gd \
    intl \
    mongodb \
    opcache \
    apcu \
    pcntl \
    pdo_mysql \
    pdo_pgsql \
    redis \
    zip \
    imap \
    ftp \
    xsl \
    sockets \
    amqp

# disable access log if configuration file exists (php-fpm)
RUN if [ -f /usr/local/etc/php-fpm.d/docker.conf ]; then \
      sed -i 's/^access.log = .*/access.log = \/dev\/null/' /usr/local/etc/php-fpm.d/docker.conf; \
      sed -i 's/^user = www-data/;user = www-data/' /usr/local/etc/php-fpm.d/www.conf; \
    fi

# get rid of notice when fpm is not running as root (php-fpm)
RUN if [ -f /usr/local/etc/php-fpm.d/www.conf ]; then \
      sed -i 's/^user = www-data/;user = www-data/' /usr/local/etc/php-fpm.d/www.conf; \
      sed -i 's/^group = www-data/;group = www-data/' /usr/local/etc/php-fpm.d/www.conf; \
    fi

# add default configuration
COPY app.ini /usr/local/etc/php/conf.d/

# set default workdir and user
WORKDIR /var/www/html
USER www-data
